import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartComponent } from './features/game/components/start/start.component';
import { GameComponent } from './features/game/components/game/game.component';
import { MainBackgroundComponent } from './features/game/components/main-background/main-background.component';


const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'game', component: MainBackgroundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
