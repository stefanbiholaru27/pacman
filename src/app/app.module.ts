import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainBackgroundComponent } from './features/game/components/main-background/main-background.component';
import { GameComponent } from './features/game/components/game/game.component';
import { StartComponent } from './features/game/components/start/start.component';

@NgModule({
  declarations: [
    AppComponent, MainBackgroundComponent, GameComponent, StartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
