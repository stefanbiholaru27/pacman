import { Position } from '../../mechanics/position.interface';
import { Enemy } from './Enemy'
import { Tile } from '../tile/Tile';
import { map, pacman, ENEMY_SPAWN_TIME } from '../../../features/game/scenes/game-scene'
import { GameMode } from '../../mechanics/modes.interface';
import { scene } from '../../../features/game/scenes/game-scene'
import { Utils } from '../../utils/utils';

export class RedGhost extends Enemy {
    private scatterPosition

    constructor() {
        let position = { x: 475, y: 375 }
        let ghost = scene.physics.add.sprite(position.x, position.y, "andrei")
        ghost.type = "Red"
        ghost.body.setSize(50, 50);
        ghost.timeToSetFree = ENEMY_SPAWN_TIME
        ghost.setScale(0.2);

        scene.enemyGroup.add(ghost);
        super(position, ghost)
        this.initialPosition = position
        this.scatterPosition = { x: 2, y: 2 }
        let newTile = this.findDestinyTile()
        this.setDestinyTile(newTile)

    }
    public override update() {
        if (this.isFree) {
            let newTile = this.findDestinyTile()
            this.setDestinyTile(newTile)
        }
        super.update()
    }

    private findDestinyTile(): Tile {

        switch (this.mode) {
            case GameMode.CHASE:
                return map.getTile(pacman.getCurrentPosition())
            case GameMode.FRIGHTENED:
                return this.frightenedTile
            case GameMode.SCATTER:
                return map.getTile(this.scatterPosition, 'index')
        }
    }

    public override freeze() {
        this.ghost.setTexture("andrei_frozen");
    }

    public override unfreeze() {
        this.ghost.setTexture("andrei");
    }
}