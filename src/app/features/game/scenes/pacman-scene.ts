export class PacmanScene extends Phaser.Scene {
    public pacman!: Phaser.Physics.Arcade.Sprite;
    public ghost1!: Phaser.Physics.Arcade.Sprite;
    public ghost2!: Phaser.Physics.Arcade.Sprite;

    constructor() {
        super({ key: 'PacmanScene' });
    }

    preload(): void {
        this.load.spritesheet('pacman', '/assets/pacmanSpriteSheet.png', {
            frameWidth: 50,
            frameHeight: 50
        });
    }

    create(): void {
        const yPos = this.scale.height - 50;
        this.pacman = this.physics.add.sprite(50, yPos, 'pacman');

        this.anims.create({
            key: 'walk_right',
            frames: this.anims.generateFrameNumbers('pacman', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'walk_up',
            frames: this.anims.generateFrameNumbers('pacman', { start: 3, end: 6 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'walk_down',
            frames: this.anims.generateFrameNumbers('pacman', { start: 6, end: 9 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'walk_left',
            frames: this.anims.generateFrameNumbers('pacman', { start: 9, end: 12 }),
            frameRate: 10,
            repeat: -1
        });


        this.pacman.play('walk_right');
        this.pacman.setVelocityX(150);
    }

    override update(): void {
        const buffer = 5;

        const atRightEdge = this.pacman.x >= this.scale.width - this.pacman.width / 2 - buffer;
        const atLeftEdge = this.pacman.x <= this.pacman.width / 2 + buffer;
        const atBottomEdge = this.pacman.y >= this.scale.height - this.pacman.height / 2 - buffer;
        const atTopEdge = this.pacman.y <= this.pacman.height / 2 + buffer;

        if (atRightEdge && atBottomEdge) {
            this.pacman!.body!.velocity.x = 0;
            this.pacman!.body!.velocity.y = -150;
            this.pacman.play('walk_up', true);
        } else if (atRightEdge && atTopEdge) {
            this.pacman!.body!.velocity.x = -150;
            this.pacman!.body!.velocity.y = 0;
            this.pacman.play('walk_left', true);
        } else if (atLeftEdge && atTopEdge) {
            this.pacman!.body!.velocity.x = 0;
            this.pacman!.body!.velocity.y = 150;
            console.log("mama")
            this.pacman.play('walk_down', true);
        } else if (atLeftEdge && atBottomEdge) {
            this.pacman!.body!.velocity.x = 150;
            this.pacman!.body!.velocity.y = 0;
            this.pacman.play('walk_right', true);
        } else {
            if (atRightEdge) {
                this.pacman!.body!.velocity.x = 0;
                this.pacman!.body!.velocity.y = -150;
                this.pacman.play('walk_up', true);
            } else if (atTopEdge) {
                this.pacman!.body!.velocity.x = -150;
                this.pacman!.body!.velocity.y = 0;
                this.pacman.play('walk_left', true);
            } else if (atLeftEdge) {
                this.pacman!.body!.velocity.x = 0;
                this.pacman!.body!.velocity.y = 150;
                this.pacman.play('walk_down', true);
            } else if (atBottomEdge) {
                this.pacman!.body!.velocity.x = 150;
                this.pacman!.body!.velocity.y = 0;
                this.pacman.play('walk_right', true);
            }
        }
    }
}
