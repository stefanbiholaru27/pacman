import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GameScene } from '../../scenes/game-scene';

@Component({
  selector: 'app-game',
  template: `
  <div #gameContainer></div>
    `,
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {
  @ViewChild('gameContainer', { static: true }) gameContainer!: ElementRef;
  game!: Phaser.Game;

  constructor() { }

  ngOnInit(): void {
    const config: Phaser.Types.Core.GameConfig = {
      type: Phaser.AUTO,
      width: 1000,
      height: 1200,
      transparent: true,
      scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        // expandParent: true,
      },
      physics: {
        default: "arcade",
        arcade: {
          debug: false,
        },
      },
      scene: GameScene,
      parent: this.gameContainer.nativeElement
    };

    this.game = new Phaser.Game(config);
  }

  ngOnDestroy(): void {
    this.game.destroy(true);
  }
}
